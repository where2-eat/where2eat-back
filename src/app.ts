const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const port = 8000;

global.__basedir = __dirname;

app.use(cors({ origin: '*' }));
// parse requests of content-type: application/json
app.use(bodyParser.json());
// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

const dbSync = require('./models/index.js');

//dbSync.sequelize.sync(); 

 dbSync.sequelize.sync({ force: true }).then(() => {
  console.log('Drop and Resync Db');
  initial();
});

const db = require('./models');
const rle = db.role;

function initial() {
  rle.create({
    id: 1,
    name: 'user',
  });

  rle.create({
    id: 2,
    name: 'moderator',
  });

  rle.create({
    id: 3,
    name: 'admin',
  });
} 

app.get('/', (req, res) => {
  res.json('Change are working!');
});

app.use('/api/user', cors(), require('./routes/user.routes.js'));
app.use('/api/auth', cors(), require('./routes/auth.routes.js'));
app.use('/api/restau', cors(), require('./routes/restau.routes.js'));
app.use('/api/image', cors(), require('./routes/uploads.routes.js'));

app.listen(port, () => {
  return console.log(`server is listening on ${port}`);
});

// http://localhost:3000/user/
