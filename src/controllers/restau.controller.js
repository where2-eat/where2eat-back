  
const db = require("../models/index.js");
const Restau = db.restau;
const Op = db.Sequelize.Op;


exports.CreateOne = (req, res) => {
  // Save User to Database
  Restau.create({
    name: req.body.name,
    type: req.body.type,
    note: req.body.note,
    lieux: req.body.lieux,
    link: req.body.link
  })
  .then((restau)=>{
    if (req.body.image) {
      res.send({ message: "Image was registered successfully!" });
    } else {
       res.send({ message: "Restau was registered successfully!" });
    }
  })
  .catch(err => {
      res.status(500).send({ message: err.message });
    });
};
// Retrieve all Restau from the database.
exports.findAll = (req, res) => {
    Restau.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Restau."
      });
    });
};

// Find a single Restau with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Restau.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Restau with id=" + id
      });
    });
};

// Find a single Restau with lieux
exports.findyByLocation = (req, res) => {
  Restau.findAll({
    where: {
      lieux:  req.params.lieux
    }
  })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Restau with lieux=" + lieux
      });
    });
};

exports.update = (req, res) => {
  const id = req.params.id;

  Restau.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Restau was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Restau with id=${id}. Maybe Restau was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Restau with id=" + id
      });
    });
};
// Delete a Restau with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Restau.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Restau was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Restau with id=${id}. Maybe Restau was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Restau with id=" + id
      });
    });
};
