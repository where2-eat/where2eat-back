const router = require("express").Router();
const { authJwt } = require("../middleware");
const controller = require("../controllers/user.controller");

router.use(function (req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});

router.get("/all", controller.allAccess);

router.get("/user", [authJwt.verifyToken], controller.userBoard);

router.get(
  "/mod",
  [authJwt.verifyToken, authJwt.isModerator],
  controller.moderatorBoard
);

router.get(
  "/admin",
  [authJwt.verifyToken, authJwt.isAdmin],
  controller.adminBoard
);

// Retrieve all user
router.get("/", [authJwt.verifyToken, authJwt.isModerator], controller.findAll);

// Retrieve a single Tutorial with idUser
router.get("/:id",  [authJwt.verifyToken, authJwt.isModerator],controller.findOne);

// Update a Tutorial with idUser
router.put("/:id",  [authJwt.verifyToken, authJwt.isModerator], controller.update);

// Delete a Tutorial with idUser
router.delete("/:id",  [authJwt.verifyToken, authJwt.isAdmin], controller.delete);

module.exports = router;
