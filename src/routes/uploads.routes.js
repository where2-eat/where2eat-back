const router = require('express').Router();
const uploadController = require("../controllers/uploads.controller");
const { authJwt } = require("../middleware");
const upload = require("../middleware/uploads");

router.post("/", [authJwt.verifyToken], upload.single("file"), uploadController.uploadFiles);

module.exports = router;