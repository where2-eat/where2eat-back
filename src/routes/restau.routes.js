const router = require("express").Router();
const { authJwt } = require("../middleware");
const controller = require("../controllers/restau.controller");

router.use(function (req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});

router.post("/", [authJwt.verifyToken], controller.CreateOne);

// Retrieve all restaus
router.get("/" , [authJwt.verifyToken],controller.findAll);

// Retrieve a single restau with idUser
router.get("/:id", [authJwt.verifyToken] ,controller.findOne);

// Retrieve a single restau with idUser
router.get("/lieux/:lieux", [authJwt.verifyToken],controller.findyByLocation);

// Update a restau with idUser
router.put("/:id",   [authJwt.verifyToken], controller.update);

// Delete a restau with idUser
router.delete("/:id", [authJwt.verifyToken], controller.delete);

module.exports = router;
