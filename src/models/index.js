const config = require("../config/db.config.ts");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(
  config.DB,
  config.USER,
  config.PASSWORD,
  {
    host: config.HOST,
    dialect: config.dialect,

    pool: {
      max: config.pool.max,
      min: config.pool.min,
      acquire: config.pool.acquire,
      idle: config.pool.idle
    }
  }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("./user.model.ts")(sequelize, Sequelize);
db.role = require("./role.model.ts")(sequelize, Sequelize);
db.image = require("./image.model.ts")(sequelize, Sequelize);
db.restau = require("./restau.model.ts")(sequelize, Sequelize);
db.refreshToken = require("./refreshToken.model.ts")(sequelize, Sequelize);

db.role.belongsToMany(db.user, {
  through: "user_roles",
  foreignKey: "roleId",
  otherKey: "userId"
});
db.user.belongsToMany(db.role, {
  through: "user_roles",
  foreignKey: "userId",
  otherKey: "roleId"
});

db.image.belongsToMany(db.restau,{
  through: "image_restaus",
  foreignKey: "imageId",
  otherKey: "restauId"
});

db.restau.belongsToMany(db.image,{
  through: "image_restaus",
  foreignKey: "restauId",
  otherKey: "imageId"
})

db.refreshToken.belongsTo(db.user, {
  foreignKey: 'userId', targetKey: 'id'
});
db.user.hasOne(db.refreshToken, {
  foreignKey: 'userId', targetKey: 'id'
});

db.ROLES = ["user", "admin", "moderator"];

module.exports = db;