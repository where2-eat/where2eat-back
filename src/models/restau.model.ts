module.exports = (sequelize, Sequelize) => {
    const Restau = sequelize.define("restaus", {
      name: {
        type: Sequelize.STRING
      },
      type: {
        type: Sequelize.STRING
      },     
      lieux: {
        type: Sequelize.STRING
      },
      note: {
        type: Sequelize.STRING
      },
      link: {
        type: Sequelize.STRING
      }
    });
  
    return Restau;
  };